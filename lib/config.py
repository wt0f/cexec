import sys
import os
import re
import yaml

from util import *

from pprint import pprint

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

def read_config(name):
    if not os.path.exists(config_file_path(name)):
        print("{} group is not valid".format(group))
        sys.exit(1)
    return yaml.load(open(config_file_path(name)), Loader=Loader)

def write_config(name, data):
    filename = config_file_path(name)
    with open(filename, 'w') as fp:
        fp.write(yaml.dump(data, Dumper=Dumper))

def load_config(name):
    for f in config_filenames():
        data = read_config(f)
        if name in data['executables']:
            return data
    return {}

def filename_for_group(name):
    for f in config_filenames():
        data = read_config(f)
        if data['name'] == name:
            return f
    return None

def config_filenames():
    return [f for f in os.listdir(config_dir()) if os.path.isfile(config_file_path(f))]

def group_names():
    groups = []
    for f in config_filenames():
        data = read_config(f)
        groups.append(data['name'])
    groups.sort()
    return groups

def delete_config(name):
    data = read_config(name)

    # remove executable  links
    for exec_name in data['executables']:
        unlink_exec_name(exec_name)

    filename = filename_for_group(name)
    if filename:
        os.remove(config_file_path(name))
    else:
        print("Group name ({}) is not found".format(name))
        sys.exit(1)

def relink_executables(name):
    data = read_config(name)
    for exec_name in data['executables']:
        bin_path = os.path.join(bin_dir(), exec_name)
        if not os.path.exists(bin_path):
            link_exec_name(exec_name)